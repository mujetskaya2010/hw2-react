import React, { Component } from "react";
import Cart from "./Cart";
import Favorites from "./Favorites";
import "./Header.scss";
export class Header extends Component {
  render() {
    return (
      <div>
        <nav className="navbar ">
          <div className="container-fluid">
            <img
              className="navbar-brand"
              src="https://www.seekpng.com/png/full/308-3085942_barbie-logo-png-download-barbie-logo-png.png"
              alt="Barbie-logo"
              width={200}
            />

            <div className="header-icons">
              {" "}
              <Cart
                cartList={this.props.cartList}
                handleRemoveFromCart={this.props.handleRemoveFromCart}
              />
              <Favorites
                favoritesList={this.props.favoritesList}
                handleRemoveFromFavorites={this.props.handleRemoveFromFavorites}
              />
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

export default Header;
