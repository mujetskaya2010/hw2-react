import "./CardList.scss";

import Card from "./Card";
import React, { Component } from "react";
import { DataListContext } from "./DataListContext";
class CardList extends Component {
  static contextType = DataListContext;

  render() {
    const { dataList } = this.context;

    return (
      <div className="card-list container">
        {dataList.map((item) => (
          <div key={item.arctical}>
            <Card
              url={item.url}
              name={item.name}
              price={item.price}
              color={item.color}
              addToCart={this.props.addToCart}
              addToFavorites={this.props.addToFavorites}
              handleRemoveFromCart={this.props.handleRemoveFromCart}
              handleRemoveFromFavorites={this.props.handleRemoveFromFavorites}
            />
          </div>
        ))}
      </div>
    );
  }
}

export default CardList;
