import PropTypes from "prop-types";
import React, { Component } from "react";
import "./Cart.scss";
export class Cart extends Component {
  constructor(props) {
    super(props);

    this.cartList = props.cartList;
  }

  renderCartItems() {
    return this.props.cartList.map((item, index) => (
      <div className="cart m-4" key={index}>
        <div className="cart-text">
          {" "}
          <div>{item.name}</div>
          <div>{item.price} $</div>
        </div>

        <img className="cart-img" src={item.url} alt="" />
        <div
          className="cart-close"
          onClick={() => this.props.handleRemoveFromCart(index)}
        >
          X
        </div>
      </div>
    ));
  }

  render() {
    return (
      <>
        <div
          onClick={this.handleClick}
          className="header-cart position-relative"
          data-bs-toggle="offcanvas"
          data-bs-target="#offcanvasScrolling"
          aria-controls="offcanvasRight"
        >
          <img
            src="https://cdn-icons-png.flaticon.com/512/1170/1170678.png?w=1480&t=st=1686209698~exp=1686210298~hmac=95cc9da100959496b0edcea96b6385329b81f5259f40f673f5862a393b6ff391"
            alt=""
          />
          <span className="position-absolute top-0 start-100 translate-middle badge rounded-pill bg-secondary">
            {this.props.cartList.length}
          </span>
        </div>

        <div
          class="offcanvas offcanvas-end"
          tabindex="-1"
          data-bs-scroll="true"
          data-bs-backdrop="false"
          id="offcanvasScrolling"
          aria-labelledby="offcanvasRight"
        >
          <div class="offcanvas-header">
            <h5 class="offcanvas-title" id="offcanvasRightLabel">
              Cart
            </h5>
            <button
              type="button"
              class="btn-close"
              data-bs-dismiss="offcanvas"
              aria-label="Close"
            ></button>
          </div>

          <div class="offcanvas-body cart-body">{this.renderCartItems()}</div>
        </div>
      </>
    );
  }
}
Cart.propTypes = {
  favoritesList: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      price: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
};
Cart.defaultProps = {
  favoritesList: PropTypes.arrayOf(
    PropTypes.shape({
      price: 10,
      url: "No foto",
    })
  ).isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
};

export default Cart;
