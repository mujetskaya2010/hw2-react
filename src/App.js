import React, { Component } from "react";
import Header from "./components/Header";
import CardList from "./components/CardList";
import DataListProvider from "./components/DataListContext";
import PropTypes from "prop-types";
export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartList: [],
      favoritesList: [],
      isModalOpen: false,
      isFavorite: false,
    };
  }

  saveDataToLocalStorage = () => {
    localStorage.setItem("cartList", JSON.stringify(this.state.cartList));
    localStorage.setItem(
      "favoritesList",
      JSON.stringify(this.state.favoritesList)
    );
  };

  loadDataFromLocalStorage = () => {
    const cartList = JSON.parse(localStorage.getItem("cartList")) || [];
    const favoritesList =
      JSON.parse(localStorage.getItem("favoritesList")) || [];
    const isFavorite = JSON.parse(localStorage.getItem("isFavorite")) || false;

    this.setState({ cartList, favoritesList, isFavorite });
  };

  componentDidMount() {
    this.loadDataFromLocalStorage();
  }

  componentDidUpdate() {
    this.saveDataToLocalStorage();
  }
  addToCart = (card) => {
    this.setState((prevState) => ({
      cartList: [...prevState.cartList, card],
    }));
    this.saveDataToLocalStorage();
  };

  addToFavorites = (favorites) => {
    this.setState((prevState) => ({
      favoritesList: [...prevState.favoritesList, favorites],
    }));
    this.saveDataToLocalStorage();
  };

  handleRemoveFromCart = (index) => {
    this.setState((prevState) => {
      const newCartList = [...prevState.cartList];
      newCartList.splice(index, 1);
      return { cartList: newCartList };
    });
    this.saveDataToLocalStorage();
  };

  handleRemoveFromFavorites = (index) => {
    this.setState((prevState) => {
      const newFavoritesList = [...prevState.favoritesList];
      newFavoritesList.splice(index, 1);
      return { favoritesList: newFavoritesList };
    });
    this.saveDataToLocalStorage();
  };

  render() {
    return (
      <>
        <Header
          cartList={this.state.cartList}
          favoritesList={this.state.favoritesList}
          handleRemoveFromCart={this.handleRemoveFromCart}
          handleRemoveFromFavorites={this.handleRemoveFromFavorites}
        />
        <DataListProvider>
          <CardList
            handleRemoveFromFavorites={this.handleRemoveFromFavorites}
            addToFavorites={this.addToFavorites}
            addToCart={this.addToCart}
          />
        </DataListProvider>
      </>
    );
  }
}

App.propTypes = {
  cartList: PropTypes.array.isRequired,
  favoritesList: PropTypes.array.isRequired,
  handleRemoveFromCart: PropTypes.func.isRequired,
  handleRemoveFromFavorites: PropTypes.func.isRequired,
};
